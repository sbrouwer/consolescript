﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleScript
{
    internal class Inflator
    {
        public static SyntaxNode Inflate(Tokens Code)
        {
            SyntaxNode RootNode = new SyntaxNode();

            for (int i = 0; i < Code.Count; i++)
            {
                Token Token = Code[i];
                if (Token.Type == TokenType.Word)
                    RootNode.SubNodes.Add(new SyntaxNode(Token.Word));
                else if (Token.Type == TokenType.StartParen)
                {
                    i++;
                    SyntaxNode SubNode = new SyntaxNode();
                    int Skip = 0;
                    int BeginI = i, EndI = -1;
                    for (int j = i; j < Code.Count; j++)
                    {
                        Token Cursor = Code[j];
                        if (Cursor.Type == TokenType.StartParen) Skip++;
                        else if (Skip == 0 & Cursor.Type == TokenType.EndParen)
                        {
                            EndI = j;
                            break;
                        }
                        else if (Skip > 0 && Cursor.Type == TokenType.EndParen) Skip--;
                    }
                    if (EndI < 0) throw new Exception(") expected.");
                    Tokens SubSection = new Tokens();
                    for (int k = BeginI; k != EndI; k++) SubSection.Add(Code[k]);
                    RootNode.SubNodes.Add(Inflate(SubSection));
                    i = EndI;
                }
            }

            return RootNode;
        }
    }
}
