﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleScript
{
    internal delegate Words Thunk(FunctionDispatcher Dispatcher);

    internal static class Thunks
    {
        public static Thunk Literal(Words Literal)
        {
            return (Dispatch) => Literal;
        }

        public static Thunk Group(List<Thunk> Group)
        {
            return (Dispatch) =>
            {
                List<Words> Evaluated = new List<Words>();
                foreach (Thunk Thunk in Group)
                    Evaluated.Add(Thunk(Dispatch));

                Words Merged = new Words();
                foreach (Words Words in Evaluated)
                    foreach (Word Word in Words)
                        Merged.Add(Word);

                return Dispatch(Merged);
            };
        }

        public static Thunk Thunkify(SyntaxNode Root)
        {
            if (Root.Values != null) return Literal(Root.Values);

            List<Thunk> SubThunks = new List<Thunk>();
            foreach (SyntaxNode SubNode in Root.SubNodes)
                SubThunks.Add(Thunkify(SubNode));
            return Group(SubThunks);
        }
    }
}
