﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleScript
{
    internal struct SubdivisionOutput
    {
        public List<string> Tokens;
        public string Accumulator;
        public bool UsingAccumulator;

        public void Whitespace()
        {
            Flush();
            UsingAccumulator = false;
            Accumulator = default(string);
        }

        public void Punctuation(char Mark, bool Prefixed)
        {
            if (Prefixed) Tokens.Add(Mark.ToString());
            Flush();
            if (!Prefixed) Tokens.Add(Mark.ToString());
            UsingAccumulator = false;
            Accumulator = default(string);
        }

        public void Flush()
        {
            if (UsingAccumulator)
                Tokens.Add(Accumulator);
        }
    }

    internal class NormalOperationState : State<char, SubdivisionOutput>
    {
        public NormalOperationState(ref SubdivisionOutput Output)
        {
            _Output = Output;
        }

        private SubdivisionOutput _Output = new SubdivisionOutput();
        public override SubdivisionOutput Output { get { return _Output; } }

        public override State<char, SubdivisionOutput> Process(char Current)
        {
            if (Current == Tokenizer.WSpace[0] || Current == Tokenizer.WSpace[1])
            {
                _Output.Whitespace();
                return this;
            }

            if (Current == Tokenizer.SParen[0])
            {
                _Output.Punctuation(Tokenizer.SParen[0], true);
                return this;
            }

            if (Current == Tokenizer.EParen[0])
            {
                _Output.Punctuation(Tokenizer.EParen[0], false);
                return this;
            }

            if (Current == Tokenizer.SBrace[0])
            {
                BraceEscapeState NewState = new BraceEscapeState(ref _Output);
                _Output.Flush();
                _Output.UsingAccumulator = false;
                _Output.Accumulator = default(string);
                return NewState;
            }

            _Output.UsingAccumulator |= true;
            _Output.Accumulator += Current;

            return this;
        }
    }

    internal class BraceEscapeState : State<char, SubdivisionOutput>
    {
        public BraceEscapeState(ref SubdivisionOutput Output)
        {
            _Output = Output;
        }

        private SubdivisionOutput _Output = new SubdivisionOutput();
        public override SubdivisionOutput Output { get { return _Output; } }

        private int BraceSkip = 0;

        public override State<char, SubdivisionOutput> Process(char Current)
        {
            if (Current == Tokenizer.SBrace[0])
            {
                BraceSkip++;
                return this;
            }

            if (BraceSkip > 0 && Current == Tokenizer.EBrace[0])
            {
                BraceSkip--;
                return this;
            }

            if (BraceSkip == 0 && Current == Tokenizer.EBrace[0])
            {
                _Output.Flush();
                _Output.UsingAccumulator = false;
                _Output.Accumulator = default(string);
                return null;
            }

            _Output.UsingAccumulator |= true;
            _Output.Accumulator += Current;

            return this;
        }
    }

    internal class Tokenizer
    {
        public static readonly char[] WSpace = new char[2] { ' ', '\t' };
        public const string SParen = "(", EParen = ")", SBrace = "{", EBrace = "}";

        public static Tokens Tokenize(string Code)
        {
            Tokens Tokens = new Tokens();
            if (Code != null)
            {
                foreach (string Token in Subdivide(Code))
                {
                    TokenType Type = GetTokenType(Token);
                    if (Type != TokenType.Word) Tokens.Add(new Token(Type));
                    else Tokens.Add(new Token(Token));
                }
            }
            return Tokens;
        }

        public static List<string> Subdivide(string Code)
        {
            SubdivisionOutput Output = new SubdivisionOutput();

            Code += " ";
            Output.Tokens = new List<string>();
            StateStack<char, SubdivisionOutput> StateMachine = new StateStack<char, SubdivisionOutput>(new NormalOperationState(ref Output));
            foreach (char Character in Code)
                StateMachine.Process(Character);

            if (Output.UsingAccumulator)
            {
                Output.Tokens.Add(Output.Accumulator);
                Output.Accumulator = default(string);
                Output.UsingAccumulator = false;
            }

            return Output.Tokens;
        }

        private static TokenType GetTokenType(string Token)
        {
            switch (Token)
            {
                case SParen:
                    return TokenType.StartParen;
                case EParen:
                    return TokenType.EndParen;
                default:
                    return TokenType.Word;
            }
        }
    }
}
