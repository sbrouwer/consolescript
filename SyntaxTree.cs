﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleScript
{
    internal class SyntaxNode
    {
        public Words Values;
        public List<SyntaxNode> SubNodes;

        public SyntaxNode(string Word) { Values = new Words(Word); }
        public SyntaxNode() { SubNodes = new List<SyntaxNode>(); }

        public override string ToString()
        {
            return SectionToString(-1);
        }

        public string SectionToString(int Depth)
        {
            string Section = "";
            if (Values != null)
            {
                foreach (Word Value in Values)
                    Section = Section + "Depth: " + Depth + " - Value = " + Value.Identifier;
                return Section + "\n";
            }
            foreach (SyntaxNode Child in SubNodes)
            {
                Section = Section + Child.SectionToString(Depth + 1);
            }
            return Section;
        }
    }
}
