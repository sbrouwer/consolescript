﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleScript
{
    public delegate Words FunctionHandle(ScriptContext Context, Words Arguments);

    public delegate Words ContextlessFuncHandle(Words Arguments);
    public delegate Words SingleArgumentFuncHandle(ScriptContext Context, Word Argument);
    public delegate Words SingleArgumentContextlessFuncHandle(Word Argument);

    internal delegate Words FunctionDispatcher(Words Code);

    internal static class Functions
    {
        public static FunctionHandle AddContext(ContextlessFuncHandle Handle) { return (Context, Args) => Handle(Args); }
        public static FunctionHandle AddArguments(SingleArgumentFuncHandle Handle) { return (Context, Args) => Handle(Context, Args.Count > 1 ? Args[1] : Args.First()); }
        public static FunctionHandle AddArgumentsAndContext(SingleArgumentContextlessFuncHandle Handle) { return (Context, Args) => Handle(Args.Count > 1 ? Args[1] : Args.First()); }
    }

    internal static class Builtins
    {
        public static Words Quote(Words Arguments)
        {
            Words Output = new Words();
            if (Arguments.Count > 1)
                for (int i = 1; i < Arguments.Count; i++) Output.Add(Arguments[i]);
            return Output;
        }

        public static Words Set(ScriptContext Context, Words Arguments)
        {
            Words Args = Quote(Arguments);
            Word Identifier = Args.First();
            Words Value = new Words();
            if (Arguments.Count > 2)
            {
                for (int i = 1; i < Args.Count; i++)
                    Value.Add(Args[i]);
                Context.Declare(Identifier, Value);
            }
            return Value;
        }

        public static Words Get(ScriptContext Context, Words Arguments)
        {
            Words Args = Quote(Arguments);
            Word Identifier = Args.First();
            return Context.Read(Identifier);
        }

        public static Words Concatenate(Words Text)
        {
            string Output = default(string);

            if (Text.Count > 1)
            {
                for (int i = 1; i < Text.Count; i++)
                {
                    Output = Output + Text[i].Identifier + " ";
                }

                if (Output.Last() == ' ')
                    Output = Output.Substring(0, Output.Length - 1);
            }

            return new Words(Output);
        }

        public static Words Range(Words Input)
        {
            //TODO: Sanity checking
            int Min = int.Parse(Input[1].Identifier);
            int Max = int.Parse(Input[2].Identifier);
            List<int> Range = new List<int>();
            for (int X = 0; X <= Max - Min; X++)
                Range.Add(X + Min);
            Words Output = new Words();
            foreach (int Num in Range)
                Output.Add(new Word(Num.ToString()));
            return Output;
        }

        public static Words Range2(Words Input)
        {
            //TODO: Sanity checking
            int MinX = int.Parse(Input[1].Identifier);
            int MinY = int.Parse(Input[2].Identifier);
            int MaxX = int.Parse(Input[3].Identifier);
            int MaxY = int.Parse(Input[4].Identifier);
            List<int> Range = new List<int>();
            for (int X = 0; X <= MaxX - MinX; X++)
            {
                for (int Y = 0; Y <= MaxY; Y++)
                {
                    Range.Add(X + MinX);
                    Range.Add(Y + MinY);
                }
            }
            Words Output = new Words();
            foreach (int Num in Range)
                Output.Add(new Word(Num.ToString()));
            return Output;
        }

        public static Words Map(ScriptContext Context, Words Input)
        {
            //TODO: Sanity checking
            Words Result = new Words();
            Words Args = Quote(Input);
            Word Function = Args.First();
            Words List = Quote(Args);
            foreach (Word Argument in List)
            {
                Words Code = new Words();
                Code.Add(Function);
                Code.Add(Argument);
                Words ThisResult = Context.ApplyFunction(Code);
                foreach (Word Output in ThisResult) Result.Add(Output);
            }
            return Result;
        }

        public static Words Map2(ScriptContext Context, Words Input)
        {
            //TODO: Sanity checking
            Words Result = new Words();
            Words Args = Quote(Input);
            Word Function = Args.First();
            Words FuncArgs = Quote(Args);
            List<Words> Arguments = new List<Words>();
            for (int i = 0; i < FuncArgs.Count; i++)
            {
                if ((i - 1) % 2 == 0)
                {
                    Words Pair = new Words();
                    Pair.Add(FuncArgs[i - 1]);
                    Pair.Add(FuncArgs[i]);
                    Arguments.Add(Pair);
                }
            }
            foreach (Words Argument in Arguments)
            {
                Words Code = new Words();
                Code.Add(Function);
                Code.Add(Argument.First());
                Code.Add(Argument.Last());
                Words ThisResult = Context.ApplyFunction(Code);
                foreach (Word Output in ThisResult) Result.Add(Output);
            }
            return Result;
        }

        public static Words Print(Words Text)
        {
            Words Concatenated = Concatenate(Text);
            Console.Write(Concatenated[0].Identifier);
            return new Words();
        }

        public static Words PrintLine(Words Text)
        {
            Words Concatenated = Concatenate(Text);
            Console.WriteLine(Concatenated[0].Identifier);
            return new Words();
        }
    }
}
