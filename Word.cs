﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleScript
{
    public struct Word
    {
        public string Identifier;

        public Word(string Value)
        {
            Identifier = Value;
        }
    }

    public class Words : List<Word>
    {
        public Words() : base() { }
        public Words(string Value) : this(new Word(Value)) { }
        public Words(Word Value)
            : this()
        {
            Add(Value);
        }
    }
}
