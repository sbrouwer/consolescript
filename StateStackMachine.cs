﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ConsoleScript
{
    internal abstract class State<Type, Out>
    {
        public abstract Out Output { get; }
        public abstract State<Type, Out> Process(Type Input);
    }

    internal class StateStack<Type, Out>
    {
        private Stack<State<Type, Out>> States = new Stack<State<Type, Out>>();

        public Out Output { get { return States.Peek().Output; } }

        public void Process(Type Input)
        {
            if (States.Count > 0)
            {
                State<Type, Out> Current = States.Peek();
                State<Type, Out> New = Current.Process(Input);
                if (New == null)
                    States.Pop();
                else if (New != Current)
                    States.Push(New);
            }
            else return;
        }

        public StateStack(State<Type, Out> Initial)
        {
            States.Push(Initial);
        }
    }
}
