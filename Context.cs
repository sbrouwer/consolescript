﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleScript
{
    public class ScriptContext
    {
        internal ContextData Data;

        internal Words ApplyFunction(Words Code)
        {
            if (Code.Count > 0)
                if (Data.Functions.ContainsKey(Code.First()))
                    return Data.Functions[Code.First()](this, Code);
                else throw new Exception("No such function: " + Code.First().Identifier);
            throw new Exception("Tried to apply an empty list of words");
        }

        public void Evaluate(string Code)
        {
            Tokens CodeTokens = Tokenizer.Tokenize(Code);
            SyntaxNode SyntaxTree = Inflator.Inflate(CodeTokens);
            Thunk CodeThunk = Thunks.Thunkify(SyntaxTree);

            try
            {
                CodeThunk(ApplyFunction);
            }
            catch (Exception E)
            {
                Console.WriteLine(E.Message + "\n" + E.StackTrace);

                if (Data.Functions.ContainsKey(new Word("log")))
                {
                    Words Text = new Words();
                    Text.Add(new Word("log"));
                    Text.Add(new Word(E.Message));
                    Data.Functions[new Word("log")](this, Text);
                }
            }
        }

        public void LoadBuiltins()
        {
            Define("set", Builtins.Set);
            Define("get", Builtins.Get);
            Define("concat", Builtins.Concatenate);
            Define("quote", Builtins.Quote);
            Define("'", Builtins.Quote);
            Define("print", Builtins.Print);
            Define("println", Builtins.PrintLine);
            Define("map", Builtins.Map);
            Define("map2", Builtins.Map2);
            Define("range", Builtins.Range);
            Define("range2", Builtins.Range2);
        }

        public Words Read(string Identifier) { return Read(new Word(Identifier)); }
        public Words Read(Word Identifier)
        {
            if (Data.Variables.ContainsKey(Identifier))
                return Data.Variables[Identifier];
            return new Words();
        }

        public void Declare(string Identifier, Word Value) { Declare(new Word(Identifier), Value); }
        public void Declare(Word Identifier, Word Value) { Declare(Identifier, new Words(Value)); }
        public void Declare(string Identifier, Words Value) { Declare(new Word(Identifier), Value); }
        public void Declare(Word Identifier, Words Value)
        {
            if (Data.Variables.ContainsKey(Identifier))
                Data.Variables.Remove(Identifier);
            Data.Variables.Add(Identifier, Value);
        }

        public void Define(string Identifier, SingleArgumentFuncHandle Function) { Define(new Word(Identifier), Function); }
        public void Define(Word Identifier, SingleArgumentFuncHandle Function) { Define(Identifier, Functions.AddArguments(Function)); }
        public void Define(string Identifier, SingleArgumentContextlessFuncHandle Function) { Define(new Word(Identifier), Function); }
        public void Define(Word Identifier, SingleArgumentContextlessFuncHandle Function) { Define(Identifier, Functions.AddArgumentsAndContext(Function)); }
        public void Define(string Identifier, ContextlessFuncHandle Function) { Define(new Word(Identifier), Function); }
        public void Define(Word Identifier, ContextlessFuncHandle Function) { Define(Identifier, Functions.AddContext(Function)); }
        public void Define(string Identifier, FunctionHandle Function) { Define(new Word(Identifier), Function); }
        public void Define(Word Identifier, FunctionHandle Function)
        {
            Data.Functions.Add(Identifier, Function);
        }

        public ScriptContext()
        {
            Data = new ContextData();
        }
    }

    internal class ContextData
    {
        public Dictionary<Word, FunctionHandle> Functions = new Dictionary<Word, FunctionHandle>();
        public Dictionary<Word, Words> Variables = new Dictionary<Word, Words>();
    }
}
