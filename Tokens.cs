﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleScript
{
    internal enum TokenType
    {
        Word,
        StartParen,
        EndParen
    }

    internal struct Token
    {
        public TokenType Type;
        public string Word;

        public Token(string Word)
        {
            Type = TokenType.Word;
            this.Word = Word;
        }

        public Token(TokenType Type)
        {
            this.Type = Type;
            Word = default(string);
        }
    }

    internal class Tokens : List<Token>
    {
        public Tokens() : base() { }
    }
}
